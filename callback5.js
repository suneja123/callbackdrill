const path = require("path");
const boardsData = require(path.join(__dirname, "./callbackData/boards.json"));
const listData = require(path.join(__dirname, "./callbackData/lists.json"));
const cardsData = require(path.join(__dirname, "./callbackData/cards.json"));
module.exports = function ThanosData(boardsInformation, listsInformation, cardsInformation, nameOfboard, cardName) {
    boardId = boardsData.find(obj => {
        if (obj.name === nameOfboard) {
            return obj;
        }
    })

    setTimeout(() => {
        boardsInformation(boardsData, boardId.id, ((err, boardData) => {
            if (err) {
                console.log("board error " + err);
            }
            else {

                listsInformation(listData, boardData["id"], ((err, listData) => {
                    if (err) {
                        console.log("list error" + err);
                    }
                    else {
                        let mindId = [];
                        listData.forEach(obj => {
                            for (let name of cardName) {
                                if (obj.name === name) {
                                    mindId.push(obj.id);

                                }
                            }
                        });
                        //console.log(mindId);
                        mindId.forEach(id => {

                            cardsInformation(cardsData, id, ((err, data) => {
                                if (err) {
                                    console.log(err);

                                }
                                else {
                                    console.log(data);
                                }
                            }))
                        })

                    }
                }))
            }

        }))

    }, 2000);

}

