


const path = require("path");
const boardsData = require(path.join(__dirname, "./callbackData/boards.json"));
const listData = require(path.join(__dirname, "./callbackData/lists.json"));
const cardsData = require(path.join(__dirname, "./callbackData/cards.json"));
module.exports = function allCardsInformation(boardsInformation, listsInformation, cardsInformation, nameOfboard) {
    boardId = boardsData.find(obj => {
        if (obj.name === nameOfboard) {
            return obj;
        }
    })

    setTimeout(() => {
        boardsInformation(boardsData, boardId.id, ((err, boardData) => {
            if (err) {
                console.log("board error " + err);
            }
            else {

                listsInformation(listData, boardData["id"], ((err, listData) => {
                    if (err) {
                        console.log("list error" + err);
                    }
                    else {
                       // console.log(listData);
                        let allInfoOfCards = [];
                        listData.forEach(obj => {
                         

                            cardsInformation(cardsData, obj.id, ((err, data) => {
                                if (err) {
                                    console.log("incards "+err+" on  "+obj.id);

                                }
                                else {
                                    allInfoOfCards.push(data);
                                    console.log(allInfoOfCards);
                                }
                            }))
                        })


                        //console.log(allInfoOfCards);
                    }
                }))
            }

        }))

    }, 2000);

}

